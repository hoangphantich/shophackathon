# Write API validate CCCD

As a user, I want to validate and find my info from CCD

Input: 001090012345 (01 - HN, 02 - SG, 090 - birthyear, 012345 -random)
Output: show information of CCID

Flow:
+ job 1: validate / validate CMT
+ job 2: call API (CCID cua chinh phu)
+ job 3: caching API tra cuu roi.

Technical:
![img.png](img.png)

Acceptance Criteria: 
+ ok
+ not ok thieu
+ not ok thua
+ not ok chu cai o giua
+ not ok chu cai khong hoa
+ not ok nhieu chu cai
