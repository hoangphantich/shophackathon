package com.microsoft.hackathon.copilotdemo.features;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

;import java.util.ArrayList;
import java.util.List;


@SpringBootTest()
@AutoConfigureMockMvc 
class CCCDInfoTests {

    @Autowired
    private MockMvc mockMvc;

		List<String> input = new ArrayList<>();
		List<String> expected = new ArrayList<>();


	/**
	 * FIXTURE / FAKER
	 */
	void init(){
			input.add("12345678A"); //ok
			expected.add("true");
		}
	@Test
	void validatedni() throws Exception {
			// ok
		mockMvc.perform(MockMvcRequestBuilders.get("/validatedni?dni=12345678A"))
			.andExpect(MockMvcResultMatchers.status().isOk())
			.andExpect(MockMvcResultMatchers.content().string("true"));

		// contain char not capital
		mockMvc.perform(MockMvcRequestBuilders.get("/validatedni?dni=12345678a"))
			.andExpect(MockMvcResultMatchers.status().isOk())
			.andExpect(MockMvcResultMatchers.content().string("false"));

		// length not enoff
		mockMvc.perform(MockMvcRequestBuilders.get("/validatedni?dni=1234567A"))
			.andExpect(MockMvcResultMatchers.status().isOk())
			.andExpect(MockMvcResultMatchers.content().string("false"));

		// lenght redundant
		mockMvc.perform(MockMvcRequestBuilders.get("/validatedni?dni=123456789A"))
			.andExpect(MockMvcResultMatchers.status().isOk())
			.andExpect(MockMvcResultMatchers.content().string("false"));

		// not ok format, 
		mockMvc.perform(MockMvcRequestBuilders.get("/validatedni?dni=12345678"))	
			.andExpect(MockMvcResultMatchers.status().isOk())
			.andExpect(MockMvcResultMatchers.content().string("false"));
	
	}
}
