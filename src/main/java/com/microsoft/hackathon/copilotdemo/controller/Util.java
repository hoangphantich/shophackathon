package com.microsoft.hackathon.copilotdemo.controller;

public class Util {
  public static boolean check(String dni) {
    if (dni == null || dni.isEmpty()) {
      return false;
    }
    String regex = "^\\d{8}[A-Z]$";

    return dni.matches(regex);
  }
}
